<?php

use App\Models\BackendMenu;
use Illuminate\Database\Seeder;

class BackendMenuTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $parent = [
            'report'       => 9,
            'hrm'          => 13,
        ];

        $menus = [
            [
                'name'      => 'Dashboard',
                'link'      => 'dashboard',
                'icon'      => 'fas fa-laptop',
                'parent_id' => 0,
                'priority'  => 9000,
                'status'    => 1,
            ],
            [
                'name'      => 'Profile',
                'link'      => 'profile',
                'icon'      => 'far fa-user',
                'parent_id' => 0,
                'priority'  => 8900,
                'status'    => 1,
            ],
            [
                'name'      => 'Departments',
                'link'      => 'departments',
                'icon'      => 'fas fa-building',
                'parent_id' => 0,
                'priority'  => 8800,
                'status'    => 1,
            ],
            [
                'name'      => 'Designations',
                'link'      => 'designations',
                'icon'      => 'fas fa-layer-group',
                'parent_id' => 0,
                'priority'  => 8700,
                'status'    => 1,
            ],
            [
                'name'      => 'Employees',
                'link'      => 'employees',
                'icon'      => 'fas fa-user-secret',
                'parent_id' => 0,
                'priority'  => 8600,
                'status'    => 1,
            ],
            [
                'name'      => 'Visitors',
                'link'      => 'visitors',
                'icon'      => 'fas fa-walking',
                'parent_id' => 0,
                'priority'  => 8600,
                'status'    => 1,
            ],
            [
                'name'      => 'Pre-registers',
                'link'      => 'pre-registers',
                'icon'      => 'fas fa-user-friends',
                'parent_id' => 0,
                'priority'  => 8600,
                'status'    => 1,
            ],
            [
                'name'      => 'Attendance',
                'link'      => 'attendance',
                'icon'      => 'fas fa-clock',
                'parent_id' => 0,
                'priority'  => 8600,
                'status'    => 1,
            ],
            [
                'name'      => 'Report',
                'link'      => '#',
                'icon'      => 'fas fa-archive',
                'parent_id' => 0,
                'priority'  => 8500,
                'status'    => 1,
            ],
            [
                'name'      => 'Visitor Report',
                'link'      => 'admin-visitor-report',
                'icon'      => 'fas fa-list-alt',
                'parent_id' => $parent['report'],
                'priority'  => 74,
                'status'    => 1,
            ],
            [
                'name'      => 'Pre-Registers Report',
                'link'      => 'admin-pre-registers-report',
                'icon'      => 'fas fa-list-alt',
                'parent_id' => $parent['report'],
                'priority'  => 74,
                'status'    => 1,
            ],
            [
                'name'      => 'Attendance Report',
                'link'      => 'attendance-report',
                'icon'      => 'fas fa-clock',
                'parent_id' => $parent['report'],
                'priority'  => 74,
                'status'    => 1,
            ],
            [
                'name'      => 'HRM',
                'link'      => '#',
                'icon'      => 'fas fa-id-card ',
                'parent_id' => 0,
                'priority'  => 81,
                'status'    => 1,
            ],
            [
                'name'      => 'Administrators',
                'link'      => 'adminusers',
                'icon'      => 'fas fa-users',
                'parent_id' => $parent['hrm'],
                'priority'  => 8400,
                'status'    => 1,
            ],
            [
                'name'      => 'Role',
                'link'      => 'role',
                'icon'      => 'fa fa-star',
                'parent_id' => $parent['hrm'],
                'priority'  => 2400,
                'status'    => 1,
            ],
            [
                'name'      => 'Settings',
                'link'      => 'setting',
                'icon'      => 'fas fa-cogs',
                'parent_id' => 0,
                'priority'  => 2400,
                'status'    => 1,
            ],
        ];

        BackendMenu::insert($menus);
    }
}
